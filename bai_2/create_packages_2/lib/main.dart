import 'package:flutter/material.dart';
import 'package:area/area.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Packages Demo',
      home: PackageScreen(),
    );
  }
}

class PackageScreen extends StatefulWidget {
  @override
  _PackageScreenState createState() => _PackageScreenState();
}

class _PackageScreenState extends State<PackageScreen> {
  final TextEditingController firstString = TextEditingController();
  final TextEditingController secondString = TextEditingController();
  String result = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Package App'),
      ),
      body: Column(
        children: [
          AppTextField(firstString, 'Nhập số thứ nhất'),
          AppTextField(secondString, 'Nhập số thứ hai'),
          Padding(
            padding: EdgeInsets.all(24),
          ),
          ElevatedButton(
            child: Text('Result'),
            onPressed: () {
              int firstNum = int.tryParse(firstString.text)!;
              int secondNum = int.tryParse(secondString.text)!;
              String res = sum(firstNum, secondNum);
              setState(() {
                result = res;
              });
            },
          ),
          Padding(
            padding: EdgeInsets.all(24),
          ),
          Text(result),
        ],
      ),
    );
  }
}

class AppTextField extends StatelessWidget {
  final TextEditingController controller;
  final String label;

  AppTextField(this.controller, this.label);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(24),
      child: TextField(
        controller: controller,
        decoration: InputDecoration(hintText: label),
      ),
    );
  }
}
