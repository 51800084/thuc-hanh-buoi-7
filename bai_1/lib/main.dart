import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Map Demop',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyMap(),
    );
  }

}



class MyMap extends StatefulWidget {
  const MyMap({Key? key}) : super(key: key);

  @override
  _MyMapState createState() => _MyMapState();
}


class _MyMapState extends State<MyMap> {
  late LatLng userPosition;

  Marker? _origin;
  Marker? _destination;


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Google Maps'),
      ),
      body: FutureBuilder(
        future: findUserLocation(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          return GoogleMap(
              initialCameraPosition:
                CameraPosition(target: snapshot.data, zoom: 12),
            markers: {
              if (_origin != null) _origin!,
              if (_destination != null) _destination!
            },
            onLongPress: _addMarker,
          );
        },
      ),
    );
  }
  Future<LatLng> findUserLocation() async {
    Location location = Location();
    // LocationData userLocation;
    // PermissionStatus hasPermission = await
    // location.hasPermission();
    // bool active = await location.serviceEnabled();
    // if (hasPermission == PermissionStatus.granted && active) {
    //   userLocation = await location.getLocation();
    //   userPosition = LatLng(userLocation.latitude!,
    //       userLocation.longitude!);
    // } else {
    //   userPosition = LatLng(51.5285582, -0.24167);
    // }
    // return userPosition;
    bool _serviceEnabled;
    PermissionStatus _permissionGranted;
    LocationData _locationData;
    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return userPosition = LatLng(51.5285582, -0.24167);
      }
    }
    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        return userPosition = LatLng(51.5285582, -0.24167);
      }
    }
    _locationData = await location.getLocation();
    print('User location');
    print(_locationData.latitude);
    print(_locationData.longitude);
    return userPosition = LatLng(_locationData.latitude!, _locationData.longitude!);
  }

  void _addMarker(LatLng pos) {
    if (_origin == null || (_origin != null && _destination != null)) {
      setState(() {
        _origin = Marker(
          markerId: MarkerId('origin'),
          infoWindow: InfoWindow(title: "Origin"),
          icon:
          BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueGreen),
          position: pos,
        );
        _destination = null;
      });
    } else {
      setState(() {
        _destination = Marker(
          markerId: MarkerId('destination'),
          infoWindow: InfoWindow(title: "Destination"),
          icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueBlue),
          position: pos,
        );
      });
    }
  }

}

